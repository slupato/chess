#include <iostream>
#include <vector>
#include <string>
#include <cassert> // do we need this
#include <fstream>
#include "hps/hps.h"
#include "Board.h"
#include "game.h"

int main()
{

    menu(); // fails at the moment
    Board chessboard;
    chessboard.set_game();
    chessboard.print_board();

    // chessboard.piece_to_move(0,0,4,4);

    chessboard.move(1,0, 4,0);
    chessboard.print_board();
    // check rook move

    // basic checks works. Kinda :-)
    std::cout << chessboard.check_rook_move(0,0, 2,0) << std::endl;
    if(chessboard.check_rook_move(0,0, 2,0))
    {
        chessboard.move(0,0, 2,0);
    }
    chessboard.print_board();

    std::cout << "Saving test" << std::endl;
    chessboard.write_file("Version_1.txt");

    // Load saved file

    Board oldgame;
    oldgame.read_file("Version_1.txt");
    std::cout << "Your loaded game..." << std::endl;
    oldgame.print_board();

    //test


    return 0;
}