#pragma once
#include <iostream>
#include <string>
#include <cassert> // do we need this
#include <fstream>
#include <string>
#include "hps/hps.h"
#include <vector>
#include <sstream>
// Define white chess pieces as unicode characters
#define wk "\u2654" // king
#define wq "\u2655" // queen
#define wr "\u2656" // rook
#define wb "\u2657" // bishop
#define wh "\u2658" // knight
#define wp "\u2659" // pawn

//Define black chess pieces as unicode characters
#define bk "\u265A" // king
#define bq "\u265B" // queen
#define br "\u265C" // rook
#define bb "\u265D" // bishop
#define bh "\u265E" // knight
#define bp "\u265F" // pawn

#define be " "    // empty empty
#define we "\u2591" // empty filled

const int MAX_ROW{8};
const int MAX_COL{8};

class Board
{
    struct BOARDSTRUCT
    {
        std::string name; // std::string name;
        int row;
        int col;
    } bi;

    BOARDSTRUCT bs[8][8];

    public:
    std::string name;
    int row;
    int col;

    bool turn{true}; // White start.

    std::vector<std::string> transfer; 
    
    
    void print_board(); 
    void set_game();

    void write_file(std::string filename);
    void read_file(std::string filename);

    void set_item_to_position(std::string name, int row, int col);

    void move(int from_row, int from_col, int to_row, int to_col);

    void piece_to_move(int from_row, int from_col, int to_row, int to_col);

    bool valid_move = false;

    bool is_empty(int row, int col);
    bool check_limits(int row, int col);
    bool is_opponent_piece(int row, int col);

    bool check_rook_move(int from_row, int from_col, int to_row, int to_col);
    bool check_bishop_move(int from_row, int from_col, int to_row, int to_col);
    bool check_knight_move(int from_row, int from_col, int to_row, int to_col);
    bool check_king_move(int from_row, int from_col, int to_row, int to_col);
    bool check_wpawn_move(int from_row, int from_col, int to_row, int to_col);
    bool check_bpawn_move(int from_row, int from_col, int to_row, int to_col);
    bool check_queen_move(int from_row, int from_col, int to_row, int to_col);


    bool check_up(int from_row, int from_col, int to_row, int to_col);

    // Rainer: bool is_free_position(int row, int col) --> tarkista onko ruutu vapaan
    // Rainer: bool is_opponent_piece(int row, int col) --> true jo vastustajan nappula, false jos oma

};
