#include "Board.h"

struct move
{
    int coords[2]={};
};

void Board::write_file(std::string filename)
{
    std::string str;
    std::vector<std::string> tmp;
    for (int i = 0; i < MAX_ROW; i++)
    {
        for (int j = 0; j < MAX_COL; j++) 
        {
            str = this->bs[i][j].name;
            tmp.push_back(str);
        }
    }

    std::string serialized = hps::to_string(tmp);
    std::cout << "Writing " << serialized.size() << " Bytes" << std::endl;

    std::ofstream dataFile;
    dataFile.open(filename, std::ios::binary); //  | std::ios::app);
    dataFile << serialized << std::endl;
    dataFile.close();
}

void Board::read_file(std::string filename)
{
    std::vector<std::string> data;
    std::string line;
    std::ifstream dataFile;
    std::string filePath = filename;

    dataFile.open(filePath, std::ios::binary);

    if(dataFile.is_open())
    {
        std::string dataRead;
        while(getline(dataFile, line))
        {
            // should only be one line to read!
            data = hps::from_string<std::vector<std::string>> (line);
        }
        dataFile.close();
    }
    else
    {
        std::cout << "File read error!" <<std::endl;
    }

    for (int i = 0; i < 8 ; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            set_item_to_position(data[(j + i*8)], i, j);
        }

    }
}

void Board::set_item_to_position(std::string name, int row, int col)
{
    this->bs[row][col].name = name;
    this->bs[row][col].row = row;
    this->bs[row][col].col = col;
}

void Board::print_board()
{   
    int k=8;
    std::cout << " A B C D E F G H \n";
    for (int i = 0; i < 8; i++)
    {
        
        std::cout << k;
        for (int j = 0; j < 8; j++) 
        {
            // std::cout << bs[i][j].name;
            std::cout << this->bs[i][j].name << " ";
        }
        std::cout << k;
        k--; 
        std::cout << std::endl;
    }
    std::cout << " A B C D E F G H \n";
}

void Board::set_game()
{ 
    bool flip=true;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j <= 8; j++) 
        {
            //white others
            if (i == 0)
            {
                if (j == 0 || j == 7)
                    this->bs[i][j].name = wr;
                if (j == 1 || j == 6)
                    this->bs[i][j].name = wh;
                if (j == 2 || j == 5)
                    this->bs[i][j].name = wb;
                if (j == 3)
                    this->bs[i][j].name = wq;
                if (j == 4)
                    this->bs[i][j].name = wk;
            }
            //white pawn
            if (i == 1)
            {
                this->bs[i][j].name = wp;
            }
            //black others
            if (i == 7)
            {
                if (j == 0 || j == 7)
                    this->bs[i][j].name = br;
                if (j == 1 || j == 6)
                    this->bs[i][j].name = bh;
                if (j == 2 || j == 5)
                    this->bs[i][j].name = bb;
                if (j == 3)
                    this->bs[i][j].name = bk;
                if (j == 4)
                    this->bs[i][j].name = bq;
            }
            //black pawn print
            if (i == 6)
            {
                this->bs[i][j].name = bp;
            }
            //empty ones
            if (i < 6 && i > 1)
            {
                if (flip)
                {
                    this->bs[i][j].name = we;
                }
                else
                {
                    this->bs[i][j].name = be;
                }
            } 
            flip = !flip;
        }
    }
}

bool Board::is_empty(int row, int col)
{
    if (this->bs[row][col].name == be || this->bs[row][col].name == we)
    {
        return true;
    }
    return false;
}

void Board::move(int from_row, int from_col, int to_row, int to_col)
{
    if(from_row >=0 && from_row < 8 && to_row >= 0 && to_row < 8)
    {
        bs[to_row][to_col].name = bs[from_row][from_col].name;
        if ((from_col % 2 == 0) && (from_row % 2 == 0))
        {
            bs[from_row][from_col].name = we; 
        }
        else
        {
            bs[from_row][from_col].name = be; 
        }
    }
}

bool Board::check_limits(int row, int col)
{
    return (row >= 0) && (row < 8) && (col >= 0) && (col < 8);
}

// antaa luvan kutsua movea!! 
// tahdotussa liikesuunnasssa ei ole omaa tai vastustan napulaa välissä
// hyväksytään se, että to positio on varattu
bool Board::check_rook_move(int from_row, int from_col, int to_row, int to_col)
{
    if (!check_limits(from_row, from_col) || !check_limits(to_row, to_col))
    {
        return false;
    }
    if (from_row == to_row && from_col == to_col)
    {
        return false;
    }
    // up or down 
    if ((from_col == to_col) && ((from_row - to_row) != 0))
    {
        // implement up "down"
        if (from_row < to_row)
        {
            for (int i = from_row+1; i < to_row; i++)
            {
                // std::cout << "Down: " << i << std::endl;
                if (!is_empty(i, to_col))
                {
                    return false;
                }
            }
            return true;
        }
        // implement "up"
        // eg 7 to 4
        if (from_row > to_row)
        {
            for (int i = from_row-1; i > to_row; i--)
            {
                if (!is_empty(i, to_col))
                {
                    return false;
                }
            }
            return true;
        }

        return true;
    }
    // left or right 
    if (((from_col-to_col) != 0) && (from_row == to_row))
    {
        // implement "Right""
        if (from_col < to_col)
        {
            for (int i = from_col+1; i < to_col; i++)
            {
                // std::cout << "Down: " << i << std::endl;
                if (!is_empty(to_row, i))
                {
                    return false;
                }
            }
            return true;
        }
        // implement "left"
        if (from_row > to_row)
        {
            for (int i = from_col-1; i > to_col; i--)
            {
                if (!is_empty(to_row, i))
                {
                    return false;
                }
            }
            return true;
        }

        return true;
    }
    return false;
}
// check if the piece is opponents
bool Board::is_opponent_piece(int row, int col)
{ 
    bool opponent{false};
    if(turn)  
    {
        if (bs[row][col].name == bk || bs[row][col].name == bq || bs[row][col].name == br || bs[row][col].name == bb || bs[row][col].name == bh || bs[row][col].name == bp)
        {
            opponent = true;
        }
    }
    if(!turn)  
    {
        if (bs[row][col].name == wk || bs[row][col].name == wq || bs[row][col].name == wr || bs[row][col].name == wb || bs[row][col].name == wh || bs[row][col].name == wp)
        {
            opponent = true;
        }
    }
    return opponent;
}

bool Board::check_bishop_move(int from_row, int from_col, int to_row, int to_col)
{
    if (!check_limits(from_row, from_col) || !check_limits(to_row, to_col))
    {
        return false;
    }
    // must be same ratio
    if (abs(from_row-to_row) != abs(from_col-to_col))
    {
        return false;
    }
        //       R C
    // NE eg 4,3 2,5
    if ((from_col < to_col) && (from_row > to_row))
    {
        int tmp_row = 1;
        for (int i = from_row-1; i > to_row; i--)
        {
            if (!is_empty(i, (from_col + tmp_row++)))
            {
                return false;
            }
        }
        return true;
    }
    // NW eg. 4,3  1,0
    if ((from_col > to_col) && (from_row > to_row))
    {
        int tmp_row = 1;
        for (int i = from_col-1; i > to_col; i--)
        {
            if (!is_empty(i, (from_col - tmp_row++)))
            {
                return false;
            }
        }
        return true;
    }

    // SE eg. 2,2  5,5
    if ((from_col < to_col) && (from_row < to_row))
    {
        // int tmp_row = 1;
        for (int i = from_col+1; i > to_col; i++)
        {
            if (!is_empty(i, i))
            {
                return false;
            }
        }
        return true;
    }

    // SW eg. 1,4  4,1
    // 2,3 --> 3,2 -> still fails...
    if ((from_col < to_col) && (from_row > to_row))
    {
        int tmp_row = 1;
        for (int i = from_col+1; i < to_col; i++)
        {
            if (!is_empty(i, (to_row - tmp_row--)))
            {
                return false;
            }
        }
        return true;
    }
    return false;
}

bool Board::check_knight_move(int from_row, int from_col, int to_row, int to_col)
{
    if ((from_row == to_row) && (from_col == to_col))
    {
        return false;
    }

    if ((from_row - to_row == 2||from_col - to_col== -2) && (from_col - to_col == 1||from_col - to_col== -1))
    {
        return true;
    }

    if ((from_row - to_row == 1 ||from_col - to_col == -1) && (from_col - to_col == 2||from_col - to_col == -2))
    {
        return true;
    }

    return false;
}

bool Board::check_king_move(int from_row, int from_col, int to_row, int to_col)
{
    if (from_row == to_row && from_col == to_col)
    {
        return false;
    }
    if ((from_row - to_row) <2 && (from_col - to_col) <2 )
    {
        return true;
    }
    
    return false;
}

bool Board::check_wpawn_move(int from_row, int from_col, int to_row, int to_col)
{
    if (from_row == 2)
    {
        if ((to_row - from_row == 1 && to_col == from_col) || (to_row - from_row == 2 && to_col == from_row))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if (from_row != 2)
    {
        if ((to_row - from_row == 1 && to_col - from_col == 1) || (to_row - from_row == 1 && to_col - from_col == -1))
        {
            if (is_opponent_piece(to_row, to_col))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (to_row - from_row == 1 && from_col == to_col)
        {
            if (is_opponent_piece(to_row, to_col))
            {
                return false;
            }

            else
            {
                return true;
            }
        }
    }
    return false;
}

bool Board::check_bpawn_move(int from_row, int from_col, int to_row, int to_col)
{
    if (from_row == 7)
    {
        if ((to_row - from_row == -1 && to_col == from_col) || (to_row - from_row == -2 && to_col == from_row))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if (from_row != 7)
    {
        if ((to_row - from_row == -1 && to_col - from_col == 1) || (to_row - from_row == -1 && to_col - from_col == -1))
        {
            if (is_opponent_piece(to_row, to_col))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (to_row - from_row == -1 && from_col == to_col)
        {
            if (is_opponent_piece(to_row, to_col))
            {
                return false;
            }

            else
            {
                return true;
            }
        }
    }
    return false;
}

bool Board::check_queen_move(int from_row, int from_col, int to_row, int to_col)
{
    if (check_bishop_move(from_row, from_col, to_row, to_col) || check_rook_move(from_row, from_col, to_row, to_col))
    {
        return true;
    }

    return false;
}

void Board::piece_to_move(int from_row, int from_col, int to_row, int to_col)
{
    
    while(!valid_move)
    {
        std::string from;
        struct move pieceToMove;
        std::cout << "Choose piece to move: " << std::endl;
        std::cin >> from;
        from_row = (pieceToMove.coords[0] = (from[0] - 97));
        from_col = (pieceToMove.coords[1] = (from[1] - 49));

        std::string to;
        std::cout << "Choose where to move: " << std::endl;
        std::cin >> to;
        to_row = (pieceToMove.coords[0] = (to[0] - 97));
        to_col = (pieceToMove.coords[1] = (to[1] - 49));

        if (bs[from_row][from_col].name == be || bs[from_row][from_col].name == we || from_row < -1 || from_row > 7 || from_col < -1 || from_col > 7 || to_row < -1 || to_row > 7 || to_col < -1 || to_col > 7)
        {
            std::cout << "Input out of bounds or empty grid" << std::endl;
        }

        else if(turn)
        {
            if (bs[from_row][from_col].name != wp || bs[from_row][from_col].name != wr || bs[from_row][from_col].name != wh || bs[from_row][from_col].name != wb || bs[from_row][from_col].name != wq || bs[from_row][from_col].name != wk)
            {
                std::cout << "YOU SHALL NOT TOUCH YOUR OPPONENTS PIECES!" << std::endl;
            }

            else if (bs[from_row][from_col].name == wp)
            {
                if (check_wpawn_move(from_row, from_col, to_row, to_col))
                {
                    move(from_row, from_col, to_row, to_col);
                    turn = false;
                    valid_move = true;
                }
            }

            else if (bs[from_row][from_col].name == wr)
            {
                if (check_rook_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = false;
                        valid_move = true;
                    }
                }
            }

            else if (bs[from_row][from_col].name == wh)
            {
                if (check_knight_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = false;
                        valid_move = true;
                    }
                }
            }

            else if (bs[from_row][from_col].name == wb)
            {
                if (check_bishop_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = false;
                        valid_move = true;
                    }
                }
            }

            else if (bs[from_row][from_col].name == wq)
            {
                if (check_queen_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = false;
                        valid_move = true;
                    }
                }
            }

            else if (bs[from_row][from_col].name == wk)
            {
                if (check_king_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = false;
                        valid_move = true;
                    }
                }
            }
     
        }

        else if(!turn)
        {
            if (bs[from_row][from_col].name != bp || bs[from_row][from_col].name != br || bs[from_row][from_col].name != bh || bs[from_row][from_col].name != bb || bs[from_row][from_col].name != bq || bs[from_row][from_col].name != bk)
            {
                std::cout << "YOU SHALL NOT TOUCH YOUR OPPONENTS PIECES!";
            }

            else if (bs[from_row][from_col].name != bp)
            {
                if (check_bpawn_move(from_row, from_col, to_row, to_col))
                {
                    move(from_row, from_col, to_row, to_col);
                    turn = true;
                    valid_move = true;
                }
            }

            else if (bs[from_row][from_col].name != br)
            {
                if (check_rook_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = true;
                        valid_move = true;
                    }
                }
            }

            else if (bs[from_row][from_col].name != bh)
            {
                if (check_knight_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = true;
                        valid_move = true;
                    }
                }
            }

            else if (bs[from_row][from_col].name != bb)
            {
                if (check_bishop_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = true;
                        valid_move = true;
                    }
                }
            }

            else if (bs[from_row][from_col].name != bq)
            {
                if (check_queen_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = true;
                        valid_move = true;
                    }
                }
            }
            
            else if (bs[from_row][from_col].name != bk)
            {
                if (check_king_move(from_row, from_col, to_row, to_col))
                {
                    if (is_opponent_piece(to_row, to_col))
                    {
                        move(from_row, from_col, to_row, to_col);
                        turn = true;
                        valid_move = true;
                    }
                }
            }    
        }
    }
}
