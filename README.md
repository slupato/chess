## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Known errors](#erros)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Sections

### Title
Terminal Chess

### Short Description
Chess game played completely on terminal. Made with c++.

### Known errors

* Menu broken

* Only one file load and save

* Game loop missing

**Ok, what works:**

* Pieces check (ok, small error with Bishop:-)

* Piece movement and turn checks.

* Board structure, pieces.

* Board printing

### Install
Make sure you have gcc version 11.1.0  
cmake minimum required version 3.10  

### Usage
While in build folder  
$ cmake ../  
$ make  
$ ./main

### Maintainer(s)

Rainer Waltzer @raiwal  
Tuomas Uusi-Luomalahti @slupato  
Antti Lehtosalo @mediator246  

### Contributing

Rainer Waltzer @raiwal  
Tuomas Uusi-Luomalahti @slupato  
Antti Lehtosalo @mediator246  

### License

Using HPS library. https://github.com/jl2922/hps  
MIT License

Otherwise
Free to use
